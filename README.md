# Front - Test de compétences ! (v1.1.1)

## Introduction

Pour ce test vous devrez réaliser l’intégration de la maquette ci-jointe du format desktop au format responsive.

Le logo et tout les blocks marqués d’une croix sont à remplacer par des images en background fournies dans le dossier img.

Vous devrez faire preuve d’imagination quand aux couleurs de la page et d’ergonomie quand à la disposition de vos éléments durant le passage au responsive.


## Attention : spécification à prendre en compte !

* Le bouton «PUSH ME» devra ouvrir une modal contenant un formulaire de contact.
* Les 3 lignes composées de plusieurs blocs qui précèdent le footer sont à réaliser en pourcentage afin de respecter le même ratio pour tout les formats.
* Ces lignes sont à animer horizontalement avec l’animation de votre choix.


## Objectif !

Sur la maquette vous trouverez des bulles numérotés représentant nos attentes en terme de fonctionnalités/d'animation, en voici le descriptif :![front_test.png](https://bitbucket.org/repo/rp8okzx/images/3340786588-front_test.png)


## Nos Recommandations !

* Nous conseillons vivement l’utilisation de bootstrap et jQuery.
* Nous attendons de vous que vous utilisiez un pré-processeurs (Sass/Less).
* La mise en place d'un task runner (gulp/grunt) est un plus.


## Nos Attentes !

###Nous attendons de vous que vous alliez aussi loin que possible car seront jugés :
* Votre capacité à structurer le DOM, vos styles et l’utilisation que vous en faite.
* Votre capacité à créer des petits scripts d’animation en JS.
* Votre capacité à rechercher des informations et à vous adaptez lorsqu’un contexte est manquant (ici, les maquettes responsives).


Made with fun by [Jessy D]